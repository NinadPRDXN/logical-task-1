function pattern1() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        for (i = 1; i <= n; i++) {
            for (j=i; j < n; j++)
            {
                document.write('&nbsp;&nbsp;');
            } 
            for (j = 1; j <= ((2 * i) - 1); j++) {
                if(j == 1 || j == ((2 * i) - 1)) {
                    document.write('*');
                }
                else if (i == n){
                    if (j % 2 == 0){
                        document.write('A');
                    }
                    else {
                        document.write('*');
                    }
                }
                else {
                    if (j % 2 == 0){
                        document.write('A');
                    }
                    else {
                        document.write('*');
                    }
                }
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    }
        
}

function pattern2() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n)  && n != "") {
        for (i = 1; i <= n; i++) {
            for (j=i; j < n; j++)
            {
                document.write('&nbsp;&nbsp;');
            }
            for (j = 1; j <= ((2 * i) - 1); j++) {
                if(i == n || j == 1 || j == ((2 * i) - 1)) {
                    document.write('*');
                }
                else {
                    document.write('&nbsp;&nbsp;');
                }
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}

function pattern3() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        for (i = 1; i <= n; i++) {
            for (j = 1; j<=i; j++) {
                document.write(j ** i);
                document.write('&nbsp;&nbsp;')
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
    
}

function pattern4() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        //For upper part
        for (i = 1; i <= n; i++) {
            for (j = i; j < n; j++) {
                document.write('*');
            }
            for (j = 1; j <= ((2 * i) - 1); j++) {
                if (j == 1 || j == ((2 * i) - 1)) {
                    document.write('*');
                }
                else {
                    document.write('&nbsp;&nbsp;');
                }
            }
            for (j = i; j < n; j++) {
                document.write('*');
            }
            document.write('<br>');
        }
        //for lower part
        for (i = n - 2; i >= 1; i--) {
            for (j = 1; j <= n - i; j++) {
                document.write('*');
            }
            for (j = 1; j <= ((2 * i) - 1); j++) {
                document.write('&nbsp;&nbsp;');
            }
            for (j = 1; j <= n - i; j++) {
                document.write('*');
            }
            document.write('<br>');
        }

        for (i = 1; i <= (2 * n) - 1; i++) {
            document.write('*');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}

function pattern5() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        //Upper Part
        for (i = 1; i <= n; i++) {
            for (j = 1; j <= i; j++) {
                document.write('*');
            }
            for (j = 1; j <= n - i; j++) {
                document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
            }
            for (j = 1; j <= i; j++) {
                document.write('*');
            }
            document.write('<br>');
        }
        //Lower Part
        for (i = 1; i <= n; i++) {
            for (j = 1; j <= n - i + 1; j++ ) {
                document.write('*');
            }
            for (j = 1; j <= i - 1; j++ ) {
                document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
            }
            for (j = 1; j <= n - i + 1; j++ ) {
                document.write('*');
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}

function pattern6() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        for (i = 1; i <= n; i++) {
            for (j = 1; j<=i; j++) {
                document.write('#');
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}

function pattern7() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        for (loop = 1; loop <= n; loop++) {
            num = loop;
            for (i = loop; i <= n; i++) {
                document.write('&nbsp;&nbsp;');
            }
            for (i = 1; i <= loop; i++) {
                document.write(num);
                num++;
            }
            num--;
            num--;
            for (i = 1; i < loop; i++) {
                document.write(num);
                num--;
            }
            document.write('<br>');
        }
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}

function array() {
    var n = document.getElementById('demo').value;
    if (!isNaN(n) && n != "") {
        function subsetSums(arr, l, r, sum = 0) {
            if (l > r) 
            { 
                array_for_evaluation.push(sum);
                return;  
            }

            subsetSums(arr, l + 1, r,  sum + arr[l]); 

            subsetSums(arr, l + 1, r, sum);
        }
    
        var randomArray = [];
        var array_for_evaluation = [];
        for(let i = 0; i < 10; i++) randomArray.push(Math.floor(Math.random() * 100));
    
        document.write('Array with random variables is are as follows:<br>');
        document.write(randomArray.toString() + '<br>');
    
        array_size = randomArray.length;
        subsetSums(randomArray, 0, array_size - 1);

        array_for_evaluation.pop();

        console.log(array_for_evaluation);
    
        var closest = array_for_evaluation.reduce(function(prev, curr) {
            return (Math.abs(curr - n) < Math.abs(prev - n) ? curr : prev);
        });
    
        document.write('Number chosen by you was :' + n + '<br>');
        document.write('Closest no. after all evaluation is: ' + closest);
    }
    else {
        alert('Please enter a Integer as an Input');
    } 
}